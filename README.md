# functional/php-trampoline

Avoid blowing the call stack when doing recursive calls. This is needed because PHP does not perform tail-call optimization. https://github.com/functional-php/trampoline

[![PHPPackages Rank](http://phppackages.org/p/functional-php/trampoline/badge/rank.svg)](http://phppackages.org/p/functional-php/trampoline)

* [*Pro Functional Php Programming Application Development Strategies for Performance Optimization, Concurrency, Testability, and Code Brevity*](https://www.worldcat.org/title/pro-functional-php-programming-application-development-strategies-for-performance-optimization-concurrency-testability-and-code-brevity)
* [*PHP Reactive Programming*](https://www.worldcat.org/title/php-reactive-programming)
* [*Functional programming*](https://en.wikipedia.org/wiki/Functional_programming)
* [Functional Programming in PHP](https://google.com/search?q=Functional+Programming+in+PHP)